# GameCollecting
本專案旨在建立一個遊戲搜尋資料庫。
希望透過這個資料庫，能讓遊戲製作者、開發者甚至玩家，能夠從各種資訊尋找符合目標的遊戲，做為參考或遊玩。

### 專案內容
使用Json格式作為資料庫的早期建立方式，以便可以靈活的調整欄位，並在單一欄位可以填上複數的資料。

### 資訊欄位
- Name: 名稱
- ReleaseDate: 發佈時間
- Platform: 遊戲發佈的平台
- Genre: 遊戲類型

### 參考格式
```json
"Name": [
  "英雄聯盟",
  "Tower of Saviors"
],
"ReleaseDate": "2009/10/27",
"Platform": [
  "Microsoft Windows",
  "Mac OS X"
],
"Genre": [
  "動作即時戰略遊戲"
]
```
### 短期目標:
- 編寫Crawler蒐集資料
- 建立線上Editer作為人工資料補完的一般管道
- 編寫Json的Parser，用於統一時間格式、Platform及Genre所使用的名稱
- 編寫Merger合併前項資料為標準系統，兼具管理資料庫的功能
- 編寫Filter搜尋器，方便在資料庫中查詢
- 依照需求增加更多資料欄位，ex: Art/Theme、Developer、Publisher
- 待資料欄位確立後，建立為SQL資料庫

### 長期目標:
- 將Json資料表轉為SQL/NoSQL資料庫
- 編寫完善的Client介面，兼具查詢與編輯的功能
- 將資料庫上線為網站服務

### 歡迎提出建議及參與資料補充
